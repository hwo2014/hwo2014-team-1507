package hwo2014

import org.json4s._
import org.json4s.DefaultFormats
import java.net.Socket
import java.io.{BufferedReader, InputStreamReader, OutputStreamWriter, PrintWriter}
import org.json4s.native.Serialization
import scala.annotation.tailrec

object NoobBot extends App {
  args.toList match {
    case hostName :: port :: botName :: botKey :: _ =>
      new NoobBot(hostName, Integer.parseInt(port), botName, botKey)
    case _ => println("args missing")
  }
}

class NoobBot(host: String, port: Int, botName: String, botKey: String) {
  implicit val formats = new DefaultFormats{}
  val socket = new Socket(host, port)
  val writer = new PrintWriter(new OutputStreamWriter(socket.getOutputStream, "UTF8"))
  val reader = new BufferedReader(new InputStreamReader(socket.getInputStream, "UTF8"))
  send(MsgWrapper("join", Join(botName, botKey)))
  play

  @tailrec private def play {
    val line = reader.readLine()
    if (line != null) {
      Serialization.read[MsgWrapper](line) match {
        case MsgWrapper("carPositions", _) =>
        case MsgWrapper("gameInit", data) =>
          println("gameInit JSON:\n" + data)
          val game = data.extract[GameInit]
          /*for(i <- game.race.track.pieces.reverse) {
            println(i)
          }*/
         parsePieces(game.race.track.pieces)
        case MsgWrapper(msgType, data) =>
          println("Received: " + msgType)
      }
      send(MsgWrapper("throttle", 0.65))
      play
    }
  }

  def send(msg: MsgWrapper) {
    writer.println(Serialization.write(msg))
    writer.flush
  }
  def parsePieces(pieces: List[Piece]): Map[String,ParsedPiece] = {
    var prev = "S0" //assume start from straight
    var lane = "?"
    val rev: List[ParsedPiece] = for(i <- pieces.reverse) {
      val pp = convert(i)
      if(i.switch) {
        if(lane=="L") {
         // TODO: switch to L lane
        } else {
          // TODO: switch to R lane
        }
      }
      if(pp.angle>23) {
        pp.turn="L"
      } else if (pp.angle<23) {
        pp.turn="R"
      } else {
        pp.turn="S"
        if(prev(0)!="S") {
          if(Integer(prev(1))>2) { // full brake,long curve
            pp.brake=0
            if(prev(0)=="L") {
              pp.lanePref="R"
              lane="R"
            } else {
              pp.lanePref="L"
              lane="L"
            }
          } else {
            pp.brake=50
            if(prev(0)=="L") {
              pp.lanePref="L"
              lane="L"
            } else {
              pp.lanePref="R"
              lane="R"
            }
          }
        }
      }
      pp.repeat=Integer(prev(1))+1
      prev=pp.turn+pp.repeat

    }
    var i = 0
    for(x <- rev.reverse) {
      i+=1
      //yield ""+i+".0"->x
      println(""+i+".0 --> "+x)
    }
    None
  }
  def convert(p: Piece): ParsedPiece = {
    ParsedPiece(p.length, p.switch, p.radius, p.angle)
  }

}

case class Join(name: String, key: String)
case class Piece(length: Double = 0.0, switch: Boolean = false, radius: Integer = 0, angle: Double = 0.0)  
case class Track(id: String, name: String, pieces: List[Piece])
case class Race(track: Track)
case class GameInit(race: Race)
case class ParsedPiece(length: Double = 0.0, switch: Boolean = false, 
  radius: Integer = 0, angle: Double = 0.0,
  var repeat: Integer = 0, var lanePref: String = 0,
  var turn: String = "", var brake: Integer = 101)  

case class MsgWrapper(msgType: String, data: JValue) {
}
object MsgWrapper {
  implicit val formats = new DefaultFormats{}

  def apply(msgType: String, data: Any): MsgWrapper = {
    MsgWrapper(msgType, Extraction.decompose(data))
  }
}
