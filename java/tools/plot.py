#!/usr/bin/python

import numpy as np
import csv,os,sys
import matplotlib.pyplot as plt
import pandas
from PIL import *

def main(argv):
  filename = argv[0]

  colnames = ['X','Tick','Piece','Lane','Angle','Speed','Hardness','Throttle']

  #data = np.loadtxt('test.csv',dtype=np.str,delimiter=';',skiprows=0)
  data = pandas.read_csv(filename,names=colnames,delimiter=';')
  '''print data'''


  ticks = list(data.Tick)
  pieces = list(data.Piece)
  lanes = list(data.Lane)
  angles = list(data.Angle)
  speeds = list(data.Speed)
  hards = list(data.Hardness)
  throttles = list(data.Throttle)

  plt.plot(ticks,pieces,label='Piece',color='k')
  plt.plot(ticks,lanes,label='Lane',color='y')
  plt.plot(ticks,angles,label='Angle',color='g')
  plt.plot(ticks,speeds,label='Speed',color='b')
  plt.plot(ticks,hards,label='Hardness',color='r')
  plt.plot(ticks,throttles,label='Throttle',color='m')

  plt.xlabel('Ticks')
  plt.ylabel('Data')
  plt.title(filename)
  plt.legend()
  #plt.show()

  plt.savefig(filename+'.jpg',dpi=100)

if __name__ == "__main__":
  main(sys.argv[1:])
