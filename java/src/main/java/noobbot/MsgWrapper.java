package noobbot;

class MsgWrapper {
    public final String msgType;
    public final Object data;
    public final int gameTick;

    public Object getData(){
    	return this.data;
    }
    MsgWrapper(final String msgType, final Object data,final int gameTick) {
        this.msgType = msgType;
        this.data = data;
        this.gameTick = gameTick;
    }

    public MsgWrapper(final SendMsg sendMsg) {
        this(sendMsg.msgType(), sendMsg.msgData(),sendMsg.gameTick);
    }
}

class CarPositions {
  public class Id {
    public String name;
    public String color;
  }
  public class Lane {
    public int startLaneIndex;
    public int endLaneIndex;
  }
  public class PiecePosition {
    public double pieceIndex;
    public double inPieceDistance;
    public Lane lane;
  }
  public Id id;
  public double angle;
  public PiecePosition piecePosition;
  public double lap;
}

class GameEnd {

  public class Car {
    public String name;
    public String color;
  }
  public class Result {
    public int laps;
    public int ticks;
    public int millis;
  }
  public class Results {
    public Car car;
    public Result result;
  }
  public Results[] results;
}

class TurboAvailable {
  public double turboDurationMilliseconds;
  public double turboDUrationTicks;
  public double turboFactor;
}
