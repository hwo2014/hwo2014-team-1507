package noobbot;

import com.google.gson.Gson;

abstract class SendMsg {
    public String toJson() {
        return new Gson().toJson(new MsgWrapper(this));
    }

    protected Object msgData() {
        return this;
    }

    protected abstract String msgType();
    protected int gameTick;
}

class Join extends SendMsg {
    public final String name;
    public final String key;

    Join(final String name, final String key,final int gameTick) {
        this.name = name;
        this.key = key;
        this.gameTick = gameTick;
    }

    @Override
    protected String msgType() {
        return "join";
    }
}

class Ping extends SendMsg {
    @Override
    protected String msgType() {
        return "ping";
    }
}

class Throttle extends SendMsg {
    private double value;

    public Throttle(double value,int gameTick) {
        this.value = value;
        this.gameTick = gameTick;
    }

    @Override
    protected Object msgData() {
        return value;
    }

    @Override
    protected String msgType() {
        return "throttle";
    }
}

class Switch extends SendMsg {
    private String value;

    public Switch(String value,int gameTick) {
        this.value = value;
        this.gameTick = gameTick;
    }

    @Override
    protected Object msgData() {
        return value;
    }

    @Override
    protected String msgType() {
        return "switchLane";
    }
}

class Turbo extends SendMsg {
  private String data;

  public Turbo(String data, int gameTick) {
    this.data=data;
    this.gameTick=gameTick;
  }

  @Override
  protected Object msgData() {
    return data;
  }

  @Override
  protected String msgType() {
    return "turbo";
  }
}

class CreateRace extends SendMsg {
  public class BotId {
    public String name;
    public String key;
  }
  public class Data {
    public BotId botId = new BotId();
    public String trackName;
    public String password;
    public int carCount;
  }
  public Data data;
  public CreateRace(String botName, String key, String trackName, String password, int carCount) {
    this.data = new Data();
    this.data.botId.name = botName;
    this.data.botId.key = key;
    this.data.trackName = trackName;
    this.data.password = password;
    this.data.carCount = carCount;
  }
  @Override
  protected Object msgData() {
    return data;
  }

  @Override
  protected String msgType() {
    return "createRace";
  }
}

class JoinRace extends SendMsg {
  public class BotId {
    public String name;
    public String key;
  }
  public class Data {
    public BotId botId = new BotId();
    public String trackName;
    public String password;
    public int carCount;
  }

  public Data data;
  public JoinRace(String botName, String key, String trackName, String password, int carCount) {
    this.data = new Data();
    this.data.botId.name = botName;
    this.data.botId.key = key;
    this.data.trackName = trackName;
    this.data.password = password;
    this.data.carCount = carCount;
  }
  @Override
  protected Object msgData() {
    return data;
  }

  @Override
  protected String msgType() {
    return "joinRace";
  }
}
