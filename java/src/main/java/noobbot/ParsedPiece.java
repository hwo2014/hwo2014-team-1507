package noobbot;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import noobbot.Track.Lanes;

class ParsedPiece extends Pieces {
    public int brakeStart = 100; 
    public int accelStart = 0;
    public String pieceIndex = "0.0";
    private Map<Integer,Lane> lanes = new HashMap<>();
    public ParsedPiece nextPiece = null;
    public int longestLane = 0;

    public boolean fullBrake = false;
    public boolean fullThrottle = true;
    public int repeat = 0;
    public double laneSwitch = 0;
    public double distanceToTurn = -1.0d;
    public double nextTurnAngle = 0.0d;

    public double hardness = 50d;
    public double nextTurnHardness = 15d;
    
    public ParsedPiece(Pieces p,List<Lanes> lanes) {
        this.setLength(p.getLength());
        this.setRadius(p.getRadius());
        this.setAngle(p.getTrueAngle());
        this.setSwitch(p.isSwitch());
        for(Lanes lane:lanes){
        	Lane l = new Lane();
        	l.distanceFromCenter = lane.getDistanceFromCenter();
        	l.laneIndex = lane.getIndex();
        	if(p.getTrueAngle()>0){
        		l.lanePieceLength = (p.getTrueAngle()/360d)*2d*Math.PI*(p.getRadius()+((-1d)*(double)lane.getDistanceFromCenter()));
            //System.out.println("                   "+l.lanePieceLength);
        	} else if(p.getTrueAngle()<0){
        		l.lanePieceLength = (p.getTrueAngle()/360d)*2d*Math.PI*(p.getRadius()+(double)lane.getDistanceFromCenter());
        	} else {
        		l.lanePieceLength = p.getLength();
        	}
        	this.lanes.put(l.laneIndex,l);
        }
        this.longestLane = getLongestLane();
    }
    
    
    
    public Lane getLane(int laneIndex){
    	return lanes.get(laneIndex);
    }
    public double getLaneLength(int laneIndex){
    	return lanes.get(laneIndex).lanePieceLength;
    }
    
    public double getLength(int laneIndex){
    	return Math.abs(lanes.get(laneIndex).lanePieceLength);
    }
    
    private int getLongestLane(){
    	int longestIndex = 0;
    	double previousVal= 0;
    	for(Lane l:lanes.values()){
    		if(l.lanePieceLength>previousVal){
    			previousVal=l.lanePieceLength;
    			longestIndex = l.laneIndex;
    		}
    	}
    	return longestIndex;
    }
    
    public String toString() {
        return " L="+this.getLength()+" R="+this.getRadius()+" A="+this.getTrueAngle()+" S="+(this.isSwitch()?"t":"f")+" R="+this.repeat+" l="+this.laneSwitch+" FB="+(this.fullBrake?"t":"f")+" FT="+(this.fullThrottle?"t":"f")+" DTT="+this.distanceToTurn+" H="+this.hardness+" NTH="+this.nextTurnHardness;
    }
    
    public class Lane{
    	public int distanceFromCenter = 0;
    	public int laneIndex = 0;
    	public double lanePieceLength = 0.0d;
    }
}
