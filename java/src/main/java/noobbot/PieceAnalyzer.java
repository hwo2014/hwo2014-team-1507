package noobbot;

import java.util.Map;
import java.util.List;
import java.util.HashMap;
import java.util.ArrayList;
import java.util.Collections;

public class PieceAnalyzer {

    public static final double LANE_SELECTOR=1.0d;

    public static Map<String, ParsedPiece> parsePieces(List<Pieces> pieces, List<Track.Lanes> lanes) {
        Collections.reverse(pieces);
        List<Pieces> overflow = new ArrayList<Pieces>();
        Map<String,ParsedPiece> parsed = new HashMap<String,ParsedPiece>();
        boolean skip = true;
        ParsedPiece prev = null;
        int pieceCount = pieces.size()-1;
        double laneSwitch = 0;
        double nextPieceLaneSwitch = 0;
        double distanceToTurnCount = -1;
        double nextTurnAngle = 0.0d;
        double nextTurnHardness = 0.0d;
        for(Pieces p : pieces) {
            ParsedPiece pp = new ParsedPiece(p,lanes);
            // skip first pieces with same profile and handle them last, dont lose index!
            if(skip && (prev==null||prev.getTrueAngle()==pp.getTrueAngle())) {
              overflow.add(p);
              pieceCount--;
              prev=pp;
              continue;
            }
            if(skip) {
              skip=false;
            }

            laneSwitch = sameAnglePiece(pp,prev,laneSwitch);
            laneSwitch = fromStraightToTurn(pp,prev,laneSwitch);
            laneSwitch = fromTurnToTurn(pp,prev,laneSwitch);
            laneSwitch = fromTurnToStraight(pp,prev,laneSwitch);
            
            if(prev.getAngle()>0 && pp.getAngle()==0) {
              distanceToTurnCount = 0;
              nextTurnAngle = prev.getTrueAngle();
              nextTurnHardness = prev.hardness;
              pp.nextTurnHardness = prev.hardness;
            } else if (pp.getAngle()>0) {
              distanceToTurnCount = -1;
            } else if (distanceToTurnCount>=0) {
              distanceToTurnCount += prev.getLength();
              pp.distanceToTurn = distanceToTurnCount;
              pp.nextTurnAngle = nextTurnAngle;
              pp.nextTurnHardness = nextTurnHardness;
            }

            if(nextPieceLaneSwitch==1) {
              nextPieceLaneSwitch=0;
              pp.laneSwitch=laneSwitch;
              laneSwitch=0;
            }

            if(pp.isSwitch()) {
              nextPieceLaneSwitch=1;
            }

            if(pp.getAngle()>0) {
              pp.hardness=Math.abs(pp.getRadius()/pp.getAngle());
            }

            //pp.nextTurnHardness = prev.hardness;


            String key = ""+pieceCount+".0";
            pp.pieceIndex = key;
            parsed.put(key,pp);
            pieceCount--;
            prev=pp;
        } 
        pieceCount = pieces.size()-1;
//        System.out.println("OVERFLOW COUNT"+overflow.size());
        for(Pieces p : overflow) {
            ParsedPiece pp = new ParsedPiece(p,lanes);
            
            laneSwitch = sameAnglePiece(pp,prev,laneSwitch);
            laneSwitch = fromStraightToTurn(pp,prev,laneSwitch);
            laneSwitch = fromTurnToTurn(pp,prev,laneSwitch);
            laneSwitch = fromTurnToStraight(pp,prev,laneSwitch);
            
            if(prev.getAngle()>0 && pp.getAngle()==0) {
              distanceToTurnCount = 0;
              nextTurnAngle = prev.getTrueAngle();
              nextTurnHardness = prev.hardness;
              pp.nextTurnHardness = prev.hardness;
            } else if (pp.getAngle()>0) {
              distanceToTurnCount = -1;
            } else if (distanceToTurnCount>=0) {
              distanceToTurnCount += prev.getLength();
              pp.distanceToTurn = distanceToTurnCount;
              pp.nextTurnAngle = nextTurnAngle;
              pp.nextTurnHardness = nextTurnHardness;
            }

            if(nextPieceLaneSwitch==1) {
              nextPieceLaneSwitch=0;
              pp.laneSwitch=laneSwitch;
              laneSwitch=0;
            }

            if(pp.isSwitch()) {
              nextPieceLaneSwitch=1;
            }

            if(pp.getAngle()>0) {
              pp.hardness=Math.abs(pp.getRadius()/pp.getAngle());
            }

            //pp.nextTurnHardness = prev.hardness;

            String key = ""+pieceCount+".0";
            pp.pieceIndex = key;
            parsed.put(key,pp);
            pieceCount--;
            prev=pp;
          
        }
        return parsed;
    }
    public static void printTrack(Map<String,ParsedPiece> parsed) {
        for(int i=0;i<parsed.keySet().size();i++) {
            ParsedPiece pp = parsed.get(""+i+".0");
            System.out.println(pp.toString());
        }
    }
            public static double sameAnglePiece(ParsedPiece curr, ParsedPiece next, double laneSwitch) {
              if(curr.getTrueAngle()==next.getTrueAngle()) {
                curr.repeat=next.repeat+1;
              }
              return laneSwitch; // no lane switch required atm
            }

            public static double fromStraightToTurn(ParsedPiece curr, ParsedPiece next, double laneSwitch) {
              if (curr.getTrueAngle()==0 && next.getTrueAngle()!=0) { // from straight to turn
              if(next.repeat>=3) { // hairpin == 3
                curr.fullBrake=true;
                return PieceAnalyzer.LANE_SELECTOR*next.getTrueAngle();
              } else {
                if(laneSwitch==0) {
                  return next.getTrueAngle();
                }
              }
              }
              return laneSwitch;
            } 
            public static double fromTurnToTurn(ParsedPiece curr, ParsedPiece next, double laneSwitch) {
              if (curr.getTrueAngle()!=0 && next.getTrueAngle()!=0) { // from turn to turn
                if(next.repeat>=3) { // hairpin == 3
                  curr.fullBrake=true;
                  return PieceAnalyzer.LANE_SELECTOR*next.getTrueAngle(); // choose out lane
                } else { // just some turn
                  if(laneSwitch==0) {
                    if(curr.isSwitch()) {
                      return curr.getTrueAngle(); // choose using my angle
                    } else {
                      return next.getTrueAngle(); // choose in lane
                    }
                  }
                }
              }
              return laneSwitch;
            }
            
            public static double fromTurnToStraight(ParsedPiece curr, ParsedPiece next, double laneSwitch) {
              if (curr.getTrueAngle()!=0 && next.getTrueAngle()==0) { // from turn to straight
                curr.fullThrottle=true;
                if(curr.isSwitch()) {
                  return next.getTrueAngle(); // choose using my angle
                } else {
                  return laneSwitch;
                }
              }
              return laneSwitch;
            }
}
