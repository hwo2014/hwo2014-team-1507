package noobbot;
public class Cars {
	class Id{
		private String name;
		private String color;
		
		public String getName() {
			return name;
		}
		public void setName(String name) {
			this.name = name;
		}
		public String getColor() {
			return color;
		}
		public void setColor(String color) {
			this.color = color;
		}
	}
	
	class Dimensions{
		private double length;
		private double width;
		private double guideFlagPosition;
		public double getLength() {
			return length;
		}
		public void setLength(double length) {
			this.length = length;
		}
		public double getWidth() {
			return width;
		}
		public void setWidth(double width) {
			this.width = width;
		}
		public double getGuideFlagPosition() {
			return guideFlagPosition;
		}
		public void setGuideFlagPosition(double guideFlagPosition) {
			this.guideFlagPosition = guideFlagPosition;
		}
	}
}
