package noobbot;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.net.Socket;

import java.util.Map;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

public class Main {

    //public static int TURN_BRAKE_START=50;
    //public static int TURN_ACCEL_START=50;

    public static double FULL_BRAKE=0.0d;
    public static double BRAKE_THROTTLE=0.0d;
    public static double MID_CORNER_THROTTLE=0.5d;//0.656;
    public static double FULL_THROTTLE=1.0d;//0.755;
    public static final double ANGLE_LIMIT=25d;
    public static final double ANGLE_SLIP_LIMIT=50d;
    public static final double CORNER_SPEED_LIMIT=6.4d;
    public static final double CORNER_TURTLE_SPEED_LIMIT=3.0d; 
    public static final double INSANE_CORNER_THROTTLE=0.3d;
    public static final double HARD_CORNER_THROTTLE=0.6d;
    public static final double MEDIUM_CORNER_THROTTLE=0.8d;
    public static final double EASY_CORNER_THROTTLE=0.9d;
    public static final double BASE_SPEED=4.7d;
    public static final double HARD_FACTOR=0.25d;
    public static final double TARGET_SPEED=6.4d;

    public class RaceSettings {
      public double deAccRate = 0.0d;
      public double brakeSafety = 5d;
      public double slipAngle = 50.0d;

      public RaceSettings() {
        deAccRate = 0.0d;
        brakeSafety = 5d;
        slipAngle = 50.0d;
      }

      public String toString() {
        return "dAR="+deAccRate+" bS="+brakeSafety+" sA="+slipAngle;
      }
    }

    public static void main(String... args) throws IOException {
        String host = args[0];
        int port = Integer.parseInt(args[1]);
        String botName = args[2];
        String botKey = args[3];

        String trackName = null;
        if(args.length==5) {
          trackName = args[4];            
        }
        //System.out.println("Connecting to " + host + ":" + port + " as " + botName + "/" + botKey + (trackName!=null?" Track:"+trackName:""));

        final Socket socket = new Socket(host, port);
        final PrintWriter writer = new PrintWriter(new OutputStreamWriter(socket.getOutputStream(), "utf-8"));

        final BufferedReader reader = new BufferedReader(new InputStreamReader(socket.getInputStream(), "utf-8"));

        new Main(reader, writer, new Join(botName, botKey,0),botName, botKey, trackName);
    }

    final Gson gson = new Gson();
    private PrintWriter writer;

    public Main(final BufferedReader reader, final PrintWriter writer, final Join join, String botName, String botKey, String trackName) throws IOException {
        this.writer = writer;
        String line = null;

        if(trackName!=null) {
          createAndJoin(botName, botKey, trackName,"schumi4ever");
        } else {
          send(join);
        }

        Map<String,ParsedPiece> parsedPieces = null;
        double previousPieceIndex = 0.666d;
        int prevTick = 0;
        
        // variables for acceleration forces counting
        // movedToRaceSettings double deAccRate = 0.0d;
        RaceSettings gs = new RaceSettings();

        RaceSettings lastKnownGood = null;

        boolean crashed = false;
     
        double prevPieceLen = -1.0d;
        double prevPiecePos = 0.0d;

        int pieceTick = 1;
        double pieceTop = 0;
        double pieceTotal = 0;
        double targetSpeed = Main.TARGET_SPEED;
        double tick3Speed=0.0;
        TurboAvailable ta = null;

        double targetSpeedMod = 1.0d;
        
        while((line = reader.readLine()) != null) {
            final MsgWrapper msgFromServer = gson.fromJson(line, MsgWrapper.class);
           
            int gameTick = msgFromServer.gameTick;
            //debug(msgFromServer);
            if (msgFromServer.msgType.equals("carPositions")) {
                //debug(msgFromServer);
                final CarPositions[] cps = gson.fromJson(gson.toJson(msgFromServer.getData()), CarPositions[].class);
                //System.out.println("------P="+cp[0].piecePosition.pieceIndex+" D="+cp[0].piecePosition.inPieceDistance);
                //
                CarPositions cp = resolveCar(cps,botName);

                double ind = cp.piecePosition.pieceIndex;               
                ParsedPiece pp = parsedPieces.get(""+ind);
                double pos = cp.piecePosition.inPieceDistance;
                int lane = cp.piecePosition.lane.startLaneIndex;
                double left = thisPieceLeft(lane,pp,pos);
                
                //System.out.print(ind+"/"+pos);
                
                // only once per piece atm
                if(previousPieceIndex!=ind) {
                  /*if(ind==0.0d) {
                    System.out.println("--------------------- GT="+gameTick);
                  }*/
                  //System.out.println(" ===> top="+pieceTop+" avg("+pieceTick+")="+(pieceTotal/pieceTick));
                  pieceTop=0;
                  pieceTick=1;
                  pieceTotal=0;

                  //System.out.println(" Piece "+ind+" "+pp.toString());
                  if(previousPieceIndex!=0.666d) {
                    if(ind==0.0d) {
                      System.out.println("--------------------- GT="+gameTick);
                      if(!crashed) {
                        lastKnownGood = gs;
                        gs = moreRisk(gs);
                      }
                    }
                    prevPieceLen = parsedPieces.get(""+previousPieceIndex).getLength(lane);
                    //System.out.println(" ---> plen using "+lane+" is "+prevPieceLen);
                  } else { 
                    previousPieceIndex=parsedPieces.size()-1;
                  }
                  if(switchLane(pp,lane, gameTick)!=0) {
                    prevPieceLen = parsedPieces.get(""+previousPieceIndex).getLength(lane);
                    previousPieceIndex=ind;
                    continue;
                  }
                }

                double speed = getSpeed(prevPieceLen, prevPiecePos, pos, (gameTick-prevTick));//currentSpeed(lane,tickTimeDiff, pp, previousPiece, pos, previousPiecePosition);

                //MMA//System.out.println("left: "+left+" Speed: " + speed);
                //System.out.println(" ---> tick:"+gameTick+" pos:"+pos+" speed:"+speed + " angle:"+cp.angle);
                pieceTick++;
                pieceTotal+=speed;
                pieceTop=(speed>pieceTop?speed:pieceTop);
                previousPieceIndex=ind;

                prevPiecePos=pos;

          
                if(gs.deAccRate == 0.0d) {
                  if(gameTick==4){
                      send(new Throttle(Main.FULL_BRAKE, gameTick));
                      continue;
                  }else if(gameTick==5){
                      tick3Speed = speed;
                      send(new Throttle(Main.FULL_BRAKE, gameTick));
                      continue;
                  }else if(gameTick==6){
                      gs.deAccRate = tick3Speed-speed;
                      //System.out.println(" ---> deAccRate:"+deAccRate);
                      //send(new Throttle(Main.FULL_THROTTLE, gameTick));
                      //continue;
                  }else if(gameTick<4){
                      send(new Throttle(Main.FULL_THROTTLE, gameTick));
                      continue;
                  }
                }

                //deAccRate = 0.02d;

                if(pp.getAngle()>0) {
                  if(pp.nextTurnHardness==15d) {
                    targetSpeed = Main.BASE_SPEED+(pp.hardness*Main.HARD_FACTOR);
                  } else {
                    targetSpeed = Main.BASE_SPEED+(pp.nextTurnHardness*Main.HARD_FACTOR);
                  }
                } else {
                  targetSpeed = Main.BASE_SPEED+(pp.nextTurnHardness*Main.HARD_FACTOR);
                }
                // now the logic
                double brakeDistance = ((speed-targetSpeed)/gs.deAccRate)+gs.brakeSafety;
                double untilCorner = (pp.distanceToTurn+left);
                //System.out.println(" ---> brakeDistance:"+brakeDistance+" untilCorner:"+untilCorner);
                //System.out.println("DEACC: " + deAccRate + " MOD: "+targetSpeedMod+" TS: "+targetSpeed);

                double throttle = Main.FULL_THROTTLE;

                if(Math.abs(cp.angle)>gs.slipAngle) {
                  throttle = Main.FULL_BRAKE;
                } else if (pp.getAngle()>Main.ANGLE_LIMIT) { 
                  if (speed > Main.CORNER_SPEED_LIMIT) {
                    throttle = Main.FULL_BRAKE;
                  } else if (speed < Main.CORNER_TURTLE_SPEED_LIMIT) {
                    throttle = Main.FULL_THROTTLE;
                  } else if (pp.hardness < 1.2d) {
                    throttle = Main.INSANE_CORNER_THROTTLE;
                  } else if (pp.hardness < 2) {
                    throttle = Main.HARD_CORNER_THROTTLE;
                  } else if (pp.hardness < 4) {
                    throttle = Main.MEDIUM_CORNER_THROTTLE;
                  } else {
                    throttle = Main.EASY_CORNER_THROTTLE;
                  }  
                } else {
                  if(ta!=null && pp.getAngle()==0 && pp.repeat>3 && cp.angle<20) {
                    //System.out.println("Lets go, TURBO!");
                    send(new Turbo("WHOOOOOOOOOOOOOOOOOOOOOOOOOOOOORRRRRRRM",gameTick));
                    ta=null;
                  } else if (brakeDistance>0 && brakeDistance>=untilCorner){
                    throttle = Main.FULL_BRAKE;
                  } else {
                    throttle = Main.FULL_THROTTLE;
                  }
                }
                
                send(new Throttle(throttle*targetSpeedMod,gameTick));

                //System.out.println("00;"+gameTick+";"+ind+";"+lane+";"+Math.abs(cp.angle)+";"+speed+";"+pp.hardness+";"+throttle);
            } else if (msgFromServer.msgType.equals("join")) {
                System.out.println("Joined");
                System.out.println("I am 1205 CI");
            } else if (msgFromServer.msgType.equals("gameInit")) {
                //System.out.println("Race init");
                // initialize again!
                previousPieceIndex = 0.666d;
                prevTick = 0;
              
                prevPieceLen = -1.0d;
                prevPiecePos = 0.0d;

                pieceTick = 1;
                pieceTop = 0;
                pieceTotal = 0;

                ta = null;

                if(lastKnownGood!=null) {
                  gs = lastKnownGood;
                }

                final TrackInitDataJSON tid = gson.fromJson(line,TrackInitDataJSON.class);
                //debug(msgFromServer);
                parsedPieces = PieceAnalyzer.parsePieces(tid.getData().getRace().getTrack().getPieces(),tid.getData().getRace().getTrack().getLanes());
                //PieceAnalyzer.printTrack(parsedPieces);
                //System.out.println("Data parse complete");
            } else if (msgFromServer.msgType.equals("gameEnd")) {
                //System.out.println("Race end");
                final GameEnd gameEnds = gson.fromJson(gson.toJson(msgFromServer.getData()), GameEnd.class);
                //for (GameEnd.Results ge : gameEnds.results) {
                  //System.out.println(" car="+ge.car.name+" col="+ge.car.color+" laps="+ge.result.laps+" ticks="+ge.result.ticks);
                //}
            } else if (msgFromServer.msgType.equals("gameStart")) {
                //System.out.println("Race start @"+System.currentTimeMillis());
            } else if (msgFromServer.msgType.equals("crash")) {
                crashed=true;
                System.out.println("!crashed! "+crashed);
                // AI :D
                //targetSpeedMod *= 0.98d;
                //deAccRate *= 0.95d;
                gs = lessRisk(gs);
            } else if (msgFromServer.msgType.equals("turboAvailable")) {
                //System.out.println("TURBO!");
                ta = gson.fromJson(line,TurboAvailable.class);
            } else {
                //debug(msgFromServer);
                //FIXME: maybe this is extra? send(new Ping());
            }
            prevTick = gameTick;
        }
    }

    private int switchLane(ParsedPiece pp,int lane, int gameTick){
      //System.out.println("ll: " +ll + " lane: "+ lane);
      if(pp.laneSwitch>0) { //&& pp.isSwitch()) {
        //System.out.println("SwitchR ");
        send(new Switch("Right",gameTick));
        return 1;
      }
      if(pp.laneSwitch<0) { //&& pp.isSwitch()) {
        //System.out.println("SwitchL ");
        send(new Switch("Left",gameTick));
        return -1;
      }
      return 0;
    }
    
    private RaceSettings moreRisk(RaceSettings gs) {
      gs.deAccRate *= 1.02d;
      if(gs.brakeSafety>0) {
        gs.brakeSafety -= 1.0d;
      }
      gs.slipAngle += 0.5d;
      System.out.println("GS="+gs.toString());
      return gs;
    }
    private RaceSettings lessRisk(RaceSettings gs) {
      gs.deAccRate *= 0.95d;
      gs.brakeSafety += 1.0d;
      gs.slipAngle -= 0.5d;
      System.out.println("GS="+gs.toString());
      return gs;
    }

    private double thisPieceLeft(int lane,ParsedPiece pp,double pos){
        double l = pp.getLength(lane);
        return ((l-pos)/l)*100;
    }
    
    private double getSpeed(double prevPieceLen, double prevPos, double currPos, double tickDiff) {
      if(prevPieceLen==-1 || currPos>prevPos) { // this is first piece handling
        return (currPos-prevPos)/tickDiff;
      } else if (currPos<prevPos) { // lets assume that we cannot go whole piece between ticks
        return ((prevPieceLen-prevPos)+currPos)/tickDiff;
      } else {
        return 0;
      }
    }
    
    private void send(final SendMsg msg) {
        //System.out.println("JSON >>\n"+msg.toJson());
        writer.println(msg.toJson());
        writer.flush();
    }

    private void debug(MsgWrapper msg) {
        //Gson gson = new GsonBuilder().setPrettyPrinting().create();
        //System.out.println("JSON >>\n"+gson.toJson(msg));
    }

    private CarPositions resolveCar(CarPositions[] cps, String botName) {
      for(CarPositions cp: cps) {
        if(cp.id.name.equals(botName))
          return cp;
      }
      return null;
    }
    private void createAndJoin(String botName, String key,String trackName, String password) {
      send(new CreateRace(botName,key,trackName,password,1));
      send(new JoinRace(botName,key,trackName,password,1));
    }

}
