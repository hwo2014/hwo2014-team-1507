package noobbot;

import java.util.List;


public class Track {
	private String id;
	private String name;
	
	private List<Pieces> pieces;
	private List<Lanes> lanes;
	private StartingPoint startingPoint;
	
	
	public class Lanes{
		private int distanceFromCenter;
		private int index;
		
		public int getDistanceFromCenter() {
			return distanceFromCenter;
		}
		public void setDistanceFromCenter(int distanceFromCenter) {
			this.distanceFromCenter = distanceFromCenter;
		}
		public int getIndex() {
			return index;
		}
		public void setIndex(int index) {
			this.index = index;
		}
	}

	public List<Pieces> getPieces() {
		return pieces;
	}

	public void setPieces(List<Pieces> pieces) {
		this.pieces = pieces;
	}

	public List<Lanes> getLanes() {
		return lanes;
	}

	public void setLanes(List<Lanes> lanes) {
		this.lanes = lanes;
	}

	public StartingPoint getStartingPoint() {
		return startingPoint;
	}

	public void setStartingPoint(StartingPoint startingPoint) {
		this.startingPoint = startingPoint;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}
}
