package noobbot;
import java.util.List;


public class Race {
	private Track track;
	private List<Cars> cars;
	private RaceSession raceSesssion;
	
	public Track getTrack() {
		return track;
	}
	public void setTrack(Track track) {
		this.track = track;
	}
	public List<Cars> getCars() {
		return cars;
	}
	public void setCars(List<Cars> cars) {
		this.cars = cars;
	}
	public RaceSession getRaceSesssion() {
		return raceSesssion;
	}
	public void setRaceSesssion(RaceSession raceSesssion) {
		this.raceSesssion = raceSesssion;
	}
}
