package noobbot;

import com.google.gson.annotations.SerializedName;

	public class Pieces{
		private double length = 0;
		@SerializedName("switch")
		private boolean isSwitch = false;
		private double radius = 0;
		private double angle = 0;
		
		public double getLength() {
			return length;
		}
		public void setLength(double length) {
			this.length = length;
		}
		public boolean isSwitch() {
			return isSwitch;
		}
		public void setSwitch(boolean isSwitch) {
			this.isSwitch = isSwitch;
		}
		public double getRadius() {
			return radius;
		}
		public void setRadius(double radius) {
			this.radius = radius;
		}
		public double getAngle() {
			return Math.abs(angle);
		}
    public double getTrueAngle() {
      return angle;
    }
		public void setAngle(double angle) {
			this.angle = angle;
		}
	}
